param(
    [switch]
    $minor,
    [switch]
    $major,
    [string]
    $sufix
)

$data = ConvertFrom-Json (Get-Content ./version -raw)

if($major) {
    $data.major += 1
    $data.minor = 0
    $data.build = 0
}
elseif($minor) {
    $data.minor += 1
    $data.build = 0
}
else {
    $data.build += 1
}

$version = '' + $data.major + '.' + $data.minor + '.' + $data.build

if($sufix -ne '')
{
    $version += '-' + $sufix
}

dotnet pack -c Release -o ./artifacts /p:Version=$version

ConvertTo-Json $data | Set-Content ./version