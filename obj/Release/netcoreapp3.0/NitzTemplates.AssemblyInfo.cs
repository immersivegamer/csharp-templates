//------------------------------------------------------------------------------
// <auto-generated>
//     Generated by the MSBuild WriteCodeFragment class.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("Henry C. Nitz")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Release")]
[assembly: System.Reflection.AssemblyDescriptionAttribute("Templates to use when creating an application for Adatum Corporation.")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.5.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.5")]
[assembly: System.Reflection.AssemblyProductAttribute("NitzTemplates")]
[assembly: System.Reflection.AssemblyTitleAttribute("NitzTemplates")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.5.0")]
