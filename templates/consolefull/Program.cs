﻿using System;
using System.Collections.Generic;
using ByteSizeLib;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace consolefull
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = LoadConfiguration(args);
            var logger = GetLogger(config.root);

            logger.Information("Hello World!!!");
            logger.Information("App Config Loaded Value: {value}", config.app.Value);
            logger.Information("Full App Config: {@config}", config.app);

            logger.Warning("Try setting the log path using the command switch '-l=logfiles");

            logger.Verbose("Try setting config.json:Serilog:MinimumLevel to 'Verbose' this log in the log file.");
        }

        static ILogger GetLogger(IConfiguration configuration, string logName = "log.txt")
        {
            var logPath = configuration.GetSection("AppConfig").GetValue<string>("LogPath", "./logs/");
            var logFilePath = System.IO.Path.Join(logPath, logName);
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .WriteTo.File(
                    path:logFilePath,
                    fileSizeLimitBytes:(long)ByteSize.FromMegaBytes(10).Bytes,
                    rollOnFileSizeLimit:true,
                    retainedFileCountLimit:10
                )
                .WriteTo.Console(
                    restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information
                )
                .CreateLogger();

            return logger;
        }

        static (IConfiguration root, AppConfig app) LoadConfiguration(string[] commandLineArgs)
        {
            // command line switches
            var _switches = new Dictionary<string, string>(){
                {"-l", "AppConfig:LogPath"},
            };

            var root = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("config.json")
                .AddCommandLine(commandLineArgs, _switches)
                .Build();

            var app = new AppConfig();
            root.Bind("AppConfig", app);

            return (root, app);
        }
    }
}
