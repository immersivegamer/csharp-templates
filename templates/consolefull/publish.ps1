param(
    [string]
    $configurationBuild = "Release",
    [switch]
    $minor,
    [switch]
    $major,
    [string]
    $sufix
)

# increment version data
$data = ConvertFrom-Json (Get-Content ./version -raw)

if($major) {
    $data.major += 1
    $data.minor = 0
    $data.build = 0
}
elseif($minor) {
    $data.minor += 1
    $data.build = 0
}
else {
    $data.build += 1
}

$version = '' + $data.major + '.' + $data.minor + '.' + $data.build

if($sufix -ne '')
{
    $version += '-' + $sufix
}

# clean output folder
Remove-Item ./artifacts -Recurse -Force

# publish
dotnet publish -c $configurationBuild -r win-x64 /p:PublishSingleFile=True /p:Version=$version /p:AssemblyVersion=$version -o ./artifacts

# save incremented version numbers
ConvertTo-Json $data | Set-Content ./version

Write-Output "Config: $configurationBuild, Version: $version"